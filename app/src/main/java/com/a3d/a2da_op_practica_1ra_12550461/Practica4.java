package com.a3d.a2da_op_practica_1ra_12550461;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Practica4 extends AppCompatActivity {
    private EditText name;
    private EditText lastName;
    private EditText age;
    private RadioButton male;
    private RadioButton female;
    private TextView objects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica4);
        name = (EditText)findViewById(R.id.name);
        lastName = (EditText)findViewById(R.id.lastName);
        age = (EditText)findViewById(R.id.age);
        male = (RadioButton)findViewById(R.id.male);
        female = (RadioButton)findViewById(R.id.female);
        objects = (TextView) findViewById(R.id.objects);
    }

    @SuppressLint("SetTextI18n")
    public void open(View v) {
        try {
            FileInputStream fileStream = openFileInput("data.obj");
            ObjectInputStream obStream = new ObjectInputStream(fileStream);
            DataObject data;
            objects.setText("");
            while ((data = (DataObject) obStream.readObject()) != null) {
                objects.append(data.getName());
                objects.append(" ");
                objects.append(data.getLastName());
                objects.append(" ");
                objects.append("" + data.getAge());
                objects.append(" ");
                objects.append(data.isMale()? "MASCULINO" : "FEMENINO");
                objects.append("\n");
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void save(View v) {
        DataObject data = new DataObject();
        data.setName(name.getText().toString());
        data.setLastName(lastName.getText().toString());
        data.setAge(Integer.parseInt(age.getText().toString()));
        data.isMale(male.isChecked());
        try {
            FileOutputStream fileStream = openFileOutput("data.obj", MODE_APPEND);
            ObjectOutputStream obStream = new ObjectOutputStream(fileStream);
            obStream.writeObject(data);
            obStream.close();
            Toast.makeText(this, "Datos guardados", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clean(View v) {
        name.setText("");
        lastName.setText("");
        age.setText("");
        male.setChecked(true);
    }
}