package com.a3d.a2da_op_practica_1ra_12550461;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Practica5 extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_CODE = 21435435;
    private EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica5);
        text = (EditText) findViewById(R.id.text);
        checkPermissions();
    }

    private void checkPermissions() {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED)
            readExternalStorage(null);
        else
            ActivityCompat.requestPermissions(this, new String[] { permission }, PERMISSION_REQUEST_CODE);
    }

    public void readExternalStorage(View v) {
        String sd = Environment.getExternalStorageDirectory().getAbsolutePath();
        try {
            FileInputStream file = new FileInputStream(sd + "/data.txt");
            InputStreamReader reader = new InputStreamReader(file);
            BufferedReader bReader = new BufferedReader(reader);
            String line;
            text.setText("");
            while ((line = bReader.readLine()) != null)
                text.append(line + '\n');
            file.close();
            bReader.close();
            Toast.makeText(this, "Archivo cargado", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE)
            readExternalStorage(null);
    }
}
