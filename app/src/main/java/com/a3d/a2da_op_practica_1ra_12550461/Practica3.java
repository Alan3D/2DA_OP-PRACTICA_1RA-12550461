package com.a3d.a2da_op_practica_1ra_12550461;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Practica3 extends AppCompatActivity {
    private EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica3);
        text = (EditText)findViewById(R.id.text);
    }

    public void openFile(View v) {
        InputStream stream;
        try {
            stream = openFileInput("test.txt");
            InputStreamReader bytes = new InputStreamReader(stream);
            BufferedReader reader = new BufferedReader(bytes);
            String line;
            while ((line = reader.readLine()) != null)
                text.append(line + '\n');
            stream.close();
            reader.close();
            Log.i("A", "aaaa");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public void saveFile(View v) {
        OutputStream stream;
        try {
            stream = openFileOutput("test.txt", 0);
            OutputStreamWriter bytes = new OutputStreamWriter(stream);
            BufferedWriter writer = new BufferedWriter(bytes);
            writer.write(text.getText().toString());
            writer.flush();
            writer.close();
            stream.close();
            Log.i("A", "bbbb");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clean(View v) {
        text.setText("");
    }
}
