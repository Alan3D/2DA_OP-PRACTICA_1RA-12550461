package com.a3d.a2da_op_practica_1ra_12550461;

import java.io.Serializable;

class DataObject implements Serializable {
    private boolean gender;
    private int age;
    private String name;
    private String lastName;

    void setName(String n) {
        name = n;
    }

    void setLastName(String ln) {
        lastName = ln;
    }

    void setAge(int a) {
        age = a;
    }

    void isMale(boolean m) {
        gender = m;
    }

    String getName() {
        return name;
    }

    String getLastName() {
        return lastName;
    }

    int getAge() {
        return age;
    }

    boolean isMale() {
        return gender;
    }
}