package com.a3d.a2da_op_practica_1ra_12550461;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Practica1 extends AppCompatActivity {
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica1);
        textView = (TextView) findViewById(R.id.view);
        openFile();
    }

    void openFile() {
        InputStream file;
        try {
            file = openFileInput("archivo.txt");
            if (file != null) {
                InputStreamReader input = new InputStreamReader(file);
                BufferedReader reader = new BufferedReader(input);
                String line;
                try {
                    while ((line = reader.readLine()) != null) {
                        textView.append(line);
                    }
                    reader.close();
                    file.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
