package com.a3d.a2da_op_practica_1ra_12550461;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void practica1(View v) {
        startActivity(new Intent(this, Practica1.class));
    }

    public void practica2(View v) {
        startActivity(new Intent(this, Practica2.class));
    }

    public void practica3(View v) {
        startActivity(new Intent(this, Practica3.class));
    }

    public void practica4(View v) {
        startActivity(new Intent(this, Practica4.class));
    }

    public void practica5_1(View v) {
        startActivity(new Intent(this, Practica5.class));
    }

    public void practica5_2(View v) {
        startActivity(new Intent(this, Practica6.class));
    }

    public void practica6(View v) {
        startActivity(new Intent(this, Practica7.class));
    }
}
