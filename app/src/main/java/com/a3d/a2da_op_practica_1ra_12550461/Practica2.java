package com.a3d.a2da_op_practica_1ra_12550461;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Practica2 extends AppCompatActivity {
    private TextView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practica2);
        view = (TextView)findViewById(R.id.text);
        openRawFile();
    }

    void openRawFile() {
        try {
            InputStream stream = getResources().openRawResource(R.raw.prueba);
            InputStreamReader bytes = new InputStreamReader(stream);
            BufferedReader reader = new BufferedReader(bytes);
            String line;
            while ((line = reader.readLine()) != null)
                view.append(line);
        } catch (IOException x) {
            Log.e("ERROR", "input", x);
        }
    }
}
